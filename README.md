## Quick Start
* Create MySQL database and create the table
### 
```
CREATE TABLE `vesna_promo_2020_invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) NOT NULL DEFAULT '',
  `origin_request` text NOT NULL,
  `invoice_file_id` varchar(255) NOT NULL DEFAULT '',
  `invoice_file_url` text NOT NULL,
  `invoice_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `invoice_file_id` (`invoice_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

* Configure database connection in the `config.ini`
* run `composer install`
* Link to save receipt locally - `/invoices/add`
* Make sure the `cache` folder has write permission.



<?php
namespace classes;

use Luracast\Restler\RestException;

class Requests
{
    /**
     * @param string $url
     * @param string|array $data
     * @param array $headers
     * @return false|string
     */
    public static function post($url, $headers, $data)
    {
        $postHeader = '';

        foreach ($headers as $headerName => $headerValue) {
            $postHeader .= $headerName . ': ' . $headerValue . PHP_EOL;
        }

        $options = array(
            'http' => array(
                'method' => 'POST',
                'content' => $data,
                'header' => $postHeader
            )
        );

        $context = stream_context_create($options);

        try {
            $result = file_get_contents($url, false, $context);
        } catch (\Exception $e) {
            throw new RestException(500, $e->getMessage());
        }

        return $result;
    }

    public static function curlPost($url, $fields)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

}

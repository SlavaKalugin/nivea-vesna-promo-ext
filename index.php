<?php
$loader = require_once './vendor/autoload.php';
$loader->setUseIncludePath(true);

use Luracast\Restler\Restler;
use Luracast\Restler\User;
use \classes\Logger;

Logger::$PATH = __DIR__ . "/cache";

$r = new Restler(true);
$r->onCall(function () use ($r) {

    $info = [
        'base'    => $r->getBaseUrl(),
        'method'  => $r->requestMethod,
        'url'     => $r->url,
        'route'   => $r->apiMethodInfo->className.'::'.$r->apiMethodInfo->methodName,
        'version' => $r->getRequestedApiVersion(),
        'data'    => $r->getRequestData(),
        'ip'      => User::getIpAddress(),
    ];

    Logger::getLogger('requests_to_api')->log($info);
});
$r->setSupportedFormats('JsonFormat', 'UploadFormat');
$r->addAPIClass('\classes\Invoices');
$r->handle();

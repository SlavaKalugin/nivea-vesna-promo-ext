<?php


namespace classes;


class Logger
{
    public static $PATH;
    protected static $loggers = array();

    protected $name;
    protected $file;
    protected $fp;

    /**
     * Logger constructor.
     * @param $name
     * @param null $file
     */
    public function __construct($name, $file = null)
    {
        $this->name = $name;
        $this->file = $file;

        $this->open();
    }

    public function open()
    {
        if (self::$PATH == null) {
            return;
        }

        $fileName = $this->file == null
            ? self::$PATH . '/' . $this->name . '_' . date("dmY") . '.log'
            : self::$PATH . '/' . $this->file;


        $this->fp = fopen($fileName, 'a+');
    }

    /**
     * @param string $name
     * @param null $file
     * @return Logger
     */
    public static function getLogger($name = 'log', $file = null)
    {
        if (!isset(self::$loggers[$name])) {
            self::$loggers[$name] = new Logger($name, $file);
        }

        return self::$loggers[$name];
    }

    /**
     * @param string $message
     * @param string $subject
     */
    public function log($message, $subject = '')
    {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        $log = '';

        $log .= '[' . date('D M d H:i:s Y', time()) . '] ';

        if ($subject != '') {
            $log .= $subject . ': ';
        }

        $log .= $message;
        $log .= PHP_EOL;

        $this->write($log);
    }

    /**
     * @param string $string
     */
    protected function write($string)
    {
        fwrite($this->fp, $string);
    }

    public function __destruct()
    {
        fclose($this->fp);
    }
}

<?php
namespace classes;

use Luracast\Restler\RestException;
use \PDO as PDO;

class Invoices
{
    const CMS_INVOICE_ADD_URL = 'https://vesna-promo.crm3d.ru/api/v3/receipt/add';
    const CMS_INVOICE_CHECK_STATUS_URL = 'https://vesna-promo.crm3d.ru/api/v3/receipt/checkStatuses';

    /**
     * Put new invoice
     *
     * @url POST /add
     *
     * @return array
     */
    public function addInvoice()
    {
        $postData = file_get_contents('php://input');
        $data = json_decode($postData, true);

        $customerId = $data['Change']['ChangedProfileData']['ConsumerId'];
        $invoiceFileId = '';
        $invoiceFileUrl = '';
        $invoiceTimestamp = '';

        $invoiceParameters = $data['Change']['ChangedProfileData']['Transactions'][0]['Parameters'];
        foreach ($invoiceParameters as $invoiceParameter) {
            switch ($invoiceParameter['Name']) {
                case 'Origin':
                    $invoiceFileId = $invoiceParameter['Value'];
                    break;
                case 'Timestamp':
                    $invoiceTimestamp = $invoiceParameter['Value'];
                    break;
                case 'Q1':
                    $invoiceFileUrl = $invoiceParameter['Value'];
                    break;
            }
        }

        $sql = 'INSERT INTO vesna_promo_2020_invoices (
                customer_id, 
                origin_request, 
                invoice_file_id, 
                invoice_timestamp, 
                invoice_file_url
                ) VALUES (
                :customer_id,
                :origin_request,
                :invoice_file_id,
                :invoice_timestamp,
                :invoice_file_url
                )';

        $stmt = Database::prepare($sql);
        $stmt->bindValue(':customer_id', $customerId, PDO::PARAM_STR);
        $stmt->bindValue(':origin_request', json_encode($data) ?: '', PDO::PARAM_STR);
        $stmt->bindValue(':invoice_file_id', $invoiceFileId, PDO::PARAM_STR);
        $stmt->bindValue(':invoice_file_url', $invoiceFileUrl, PDO::PARAM_STR);
        $stmt->bindValue(':invoice_timestamp', $invoiceTimestamp, PDO::PARAM_STR);
        $resultExt = $stmt->execute();
        $stmt->closeCursor();


        $fileMime = pathinfo($invoiceFileUrl, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $fileMime . ';base64,' . base64_encode(file_get_contents($invoiceFileUrl));
        $invoice = [
            'customer_id' => $customerId,
            'invoice_file_id' => $invoiceFileId,
            'invoice_file_base64' => $base64,
            'invoice_timestamp' => $invoiceTimestamp,
            'token' => md5($customerId . 'a8do245_unaAs')
        ];

        $resultCms = $this->sendNewInvoiceToCms($invoice);

        return ['success' => $resultCms && $resultExt];
    }

    /**
     * @param array $invoice
     * @return bool|string
     */
    public function sendNewInvoiceToCms($invoice)
    {
        Logger::getLogger('send_invoice_to_cms')->log($invoice, 'Request');
        $result = Requests::curlPost(self::CMS_INVOICE_ADD_URL, $invoice);
        Logger::getLogger('send_invoice_to_cms')->log($result, 'Response');
        $result = json_decode($result, true);

        return $result['success'];
    }

    /**
     * @param array $invoices
     * @return array
     */
    public function checkInvoicesStatus($invoices)
    {
        Logger::getLogger('check_status')->log($invoices, 'Request');
        $result = Requests::curlPost(self::CMS_INVOICE_CHECK_STATUS_URL, $invoices);
        Logger::getLogger('check_status')->log($result, 'Response');
        $result = json_decode($result, true);

        if ($result['success']) {
            return $result['receipts'];
        }

        return [];
    }

    /**
     * Get invoices for customer
     *
     * @url GET /get
     *
     * @return array
     */
    public function getCustomerInvoices()
    {
        $customerId = $this->getCustomerId();

        $sql = 'SELECT 
            customer_id,
            invoice_file_id,
            invoice_timestamp,
            invoice_file_url
            FROM vesna_promo_2020_invoices WHERE 
            customer_id = :customer_id
            ';

        $stmt = Database::prepare($sql);
        $stmt->bindValue(':customer_id', $customerId, PDO::PARAM_STR);
        $stmt->execute();

        $result = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $row['status'] = 'Проверяется';
            $row['invoice_timestamp'] = date('d.m.Y / H:i', strtotime($row['invoice_timestamp']));
            $result[$row['invoice_file_id']] = $row;
        }
        $stmt->closeCursor();

        $checkedInvoices  = $this->checkInvoicesStatus(array_keys($result));

        if (is_array($checkedInvoices)) {
            foreach ($checkedInvoices as $invoiceId => $checkedInvoice) {
                if (isset($result[$invoiceId])) {
                    $result[$invoiceId]['status'] = $checkedInvoice['status'];
                    $result[$invoiceId]['prize'] = implode(', ', $checkedInvoice['prize']);
                }
            }
        }

        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../view');
        $twig = new \Twig\Environment($loader);
        $template = $twig->load('invoices.twig');

        echo $template->render(['invoices' => $result]);

        exit();
    }

    /**
     * @return string
     */
    protected function getCustomerId()
    {
        $customer_id = '';

        if (!empty($_COOKIE['PcLogin_TklWRUEuUlUgVjd8TlZSVXN0YW5kYXJkcw=='])) {
            $user_info = $_COOKIE['PcLogin_TklWRUEuUlUgVjd8TlZSVXN0YW5kYXJkcw=='];
            $user_info = json_decode($user_info, true);
            $userEmail = $user_info['UserName'];
            // GET ConsumerID from PC
            $data = json_encode(
                [
                    'Attributes' => [
                        [
                            'Name' => 'Email',
                            'Value' => $userEmail
                        ]
                    ]
                ]
            );

            $headers = [
                'Authorization' => 'Basic VGtsV1JVRXVVbFVnVmpkOFRsWlNWWE4wWVc1a1lYSmtjdz09OmcvMjFDVmEhMzU0',
                'Content-Type' => 'application/json'
            ];
            Logger::getLogger('request_customer_id')->log($data, 'Request');
            $result = Requests::post(
                'https://consumer.procampaignapi.com/Consumer?requestConsumerId=true', $headers, $data
            );
            Logger::getLogger('request_customer_id')->log($result, 'Response');
            $result = json_decode($result, true);

            if ($result['IsSuccessful']) {
                $customer_id = $result['Data']['ConsumerId'];
            }
            $customer_id = substr($customer_id, 0, 8) . '-' . substr($customer_id, 8, 4) . '-'
                . substr($customer_id, 12, 4) . '-' . substr($customer_id, 16, 4) . '-' . substr($customer_id, 20, 12);
        }

        return $customer_id;
    }



}
